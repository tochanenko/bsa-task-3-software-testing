# BSA - Task 3 - Software Testing

### My Services:

* `getUncompleted` - returns list of uncompleted tasks;
* `uncompleteToDo` - uncompletes todo and returns it;
* `completeAll` - complete all todos and return them.

### My Mappings in `ToDoController.java`:

* `/todos/uncompleted` - `GET` - returns list of uncompleted tasks;
* `/todos/{id}/uncomplete` - `POST` - uncompletes todo with id `{id}` and returns it;
* `/todos/completeall` - `POST` - complete all todos and return them.

### Unit Tests in `ToDoServiceTest.java`:

* `whenGetUncompleted_thenReturnWithCompletedAtEqualsNull` - check if only uncompleted (those with `completedAt` equals `null`) are being returned;
* `whenUncomplete_thenReturnWithCompletedAtEqualsNull` - set `completedAt` to current time and return ToDo;
* `whenCompleteAll_thenReturnAllCompleted` - check if all were completed and returned.

### Integration Tests

Single Everything-Covering Integration Test is in `ToDoControllerAllLayers.java` file.

* `whenGetUncompleted_thenReturnWithCompletedAtEqualsNull` - check `/todos/uncompleted`;
* `whenCompleteAll_thenReturnAllCompleted` - check `/todos/completeall`;
* `whenGetRandom_thenThrowIllegalArgumentException` - check for `/todos/0/uncomplete` to return `404 Error` when there is no ToDo with id `0`.