package com.example.demo.dto.mapper;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.model.ToDoEntity;

import java.util.List;
import java.util.stream.Collectors;

public class ToDoEntityToResponseMapper {
    public static ToDoResponse map(ToDoEntity todoEntity) {
        if (todoEntity == null)
            return null;
        var result = new ToDoResponse();
        result.id = todoEntity.getId();
        result.text = todoEntity.getText();
        result.completedAt = todoEntity.getCompletedAt();
        return result;
    }

    public static List<ToDoResponse> mapToList(List<ToDoEntity> toDoEntities) {
        return toDoEntities.stream()
                .map(ToDoEntityToResponseMapper::map)
                .collect(Collectors.toList());
    }
}