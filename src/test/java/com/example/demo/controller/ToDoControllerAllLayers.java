package com.example.demo.controller;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class ToDoControllerAllLayers {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenGetAll_thenReturnAll() throws Exception {
        // call
        var objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
        var todo = new ToDoSaveRequest();
        todo.text = "Todo 1";

        MvcResult result = this.mockMvc
                .perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectWriter.writeValueAsString(todo))
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        var objectReader = new ObjectMapper().reader().forType(ToDoResponse.class);
        ToDoResponse response = objectReader.readValue(result.getResponse().getContentAsString());

        // validate
        assertNotNull(response.id);
        assertEquals(response.text, todo.text);
        assertNull(response.completedAt);
    }

}