package com.example.demo.controller;

import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoService toDoService;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        String testText = "My to do text";
        Long testId = 1L;
        when(toDoService.getAll()).thenReturn(
                Collections.singletonList(
                        ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText))
                )
        );

        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(testId))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    // MY INTEGRATION TESTS

    @Test
    void whenGetUncompleted_thenReturnWithCompletedAtEqualsNull() throws Exception {
        var testToDos = new ArrayList<ToDoEntity>();
        var todo1 = new ToDoEntity(0L, "Test 1");
        var todo2 = new ToDoEntity(1L, "Test 2");
        var todo3 = new ToDoEntity(2L, "Test 2");

        todo1.completeNow();
        todo2.completeNow();

        testToDos.add(todo1);
        testToDos.add(todo2);
        testToDos.add(todo3);

        when(toDoService.getAll()).thenReturn(ToDoEntityToResponseMapper.mapToList(testToDos));

        when(toDoService.getUncompleted()).thenReturn(
                ToDoEntityToResponseMapper.mapToList(
                        List.of(todo1, todo2)
                )
        );

        // validate
        this.mockMvc
                .perform(get("/todos/uncompleted"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].completedAt").exists())
                .andExpect(jsonPath("$[1].completedAt").exists());
    }

    @Test
    void whenCompleteAll_thenReturnAllCompleted() throws Exception {
        var startTime = ZonedDateTime.now(ZoneOffset.UTC);

        var testToDos = new ArrayList<ToDoEntity>();
        var todo1 = new ToDoEntity(0L, "Test 1");
        var todo2 = new ToDoEntity(1L, "Test 2");
        var todo3 = new ToDoEntity(2L, "Test 2");

        testToDos.add(todo1);
        testToDos.add(todo2);
        testToDos.add(todo3);

        // mock

        when(toDoService.getAll()).thenReturn(ToDoEntityToResponseMapper.mapToList(testToDos));

        when(toDoService.completeAll()).thenReturn(
                ToDoEntityToResponseMapper.mapToList(
                        List.of(todo1.completeNow(), todo2.completeNow(), todo3.completeNow())
                )
        );

        // call
        var result = toDoService.completeAll();

        // validate
        this.mockMvc
                .perform(post("/todos/completeall"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].completedAt").exists())
                .andExpect(jsonPath("$[1].completedAt").exists())
                .andExpect(jsonPath("$[2].completedAt").exists());
    }

    @Test
    void whenGetRandom_thenThrowIllegalArgumentException() throws Exception {
        when(toDoService.uncompleteToDo(anyLong())).thenThrow(ToDoNotFoundException.class);

        this.mockMvc
                .perform(post("/todos/0/uncomplete"))
                .andExpect(status().isNotFound());
    }

}